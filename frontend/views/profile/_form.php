<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($user, 'password')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female'], ['prompt' => '--  Select  --']); ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'race')->dropDownList(['Malay' => 'Malay', 'Indian' => 'Indian', 'Chineese' => 'Chineese', 'Others' => 'Others'], ['prompt' => '--  Select  --']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-4  col-sm-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-3 col-md-4  col-sm-6">
            <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-3 col-md-4  col-sm-6">
            <?= $form->field($model, 'poscode')->textInput(['maxlength' => 5]) ?>
        </div>
        <div class="col-lg-3 col-md-4  col-sm-6">
            <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?php if (Yii::$app->user->role <= common\components\User::ADMIN_SYSTEM) { ?>
        <div class="row">
            <div class="col-lg-2 col-md-2  col-sm-2">
                <?= $form->field($user, 'status')->dropDownList([common\models\User::STATUS_INACTIVE => 'INACTIVE', common\models\User::STATUS_ACTIVE => 'ACTIVE'], ['prompt' => '--  Select  --']); ?>
            </div>
        </div>
    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
