<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/default-avatar.jpg" class="img-circle" alt="User Image"/>
            </div>
            <?php if (Yii::$app->user->id && Yii::$app->user->role != 0) { ?>

                <div class="pull-left info">
                    <p><?= Yii::$app->user->name ?></p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            <?php } else { ?>
                <div class="pull-left info">
                    <p>Guest</p>

                    <a href="#"><i class="fa fa-circle text-danger"></i> Offline</a>
                </div>
            <?php } ?>
        </div>

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                        <span class="input-group-btn">
                            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>-->
        <!-- /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu delay'],
                    'items' => [
                        ['label' => 'eData', 'options' => ['class' => 'header']],
//                        if user is offline
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest, 'options' => ['class' => 'delay-child'],],
                        [
                            'label' => 'My Dashboard',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'visible' => Yii::$app->user->isUser,
                            'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'My Profile', 'url' => ['profile/view', 'id' => Yii::$app->user->id],],
                            ],
                        ],
//                        if module is car rental
                        [
                            'label' => 'Car Rental',
                            'icon' => 'fa fa-car',
                            'url' => '#',
                            'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isUser, 'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'Client Manager', 'icon' => 'fa fa-users', 'url' => '#',
                                    'items' => [
                                        ['label' => 'Clients Record', 'icon' => 'fa fa-users', 'url' => ['car-rental/all-client-record'],],
                                        ['label' => 'Register Client', 'icon' => 'fa fa-users', 'url' => ['car-rental/client-registration'],],
                                    ],
                                ],
                                ['label' => 'Asset Manager', 'icon' => 'fa fa-users', 'url' => '#',
                                    'items' => [
                                        ['label' => 'Assets Record', 'icon' => 'fa fa-users', 'url' => ['car-rental/all-asset-record'],],
                                        ['label' => 'Register Asset', 'icon' => 'fa fa-users', 'url' => ['car-rental/asset-registration'],],
                                    ],
                                ],
                                ['label' => 'Rental Manager', 'icon' => 'fa fa-users', 'url' => '#',
                                    'items' => [
                                        ['label' => 'Rental Record', 'icon' => 'fa fa-users', 'url' => ['car-rental/all-rental-record'],],
                                        ['label' => 'New Rental', 'icon' => 'fa fa-users', 'url' => ['car-rental/new-rental'],],
                                    ],
                                ],
                            ],
                        ],
//                        if user is admin
                        [
                            'label' => 'Administration',
                            'icon' => 'fa fa-dashboard',
                            'url' => '#',
                            'visible' => Yii::$app->user->isAdmin, 'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'My Profile', 'icon' => 'fa fa-user', 'url' => ['profile/view', 'id' => Yii::$app->user->id],],
                                ['label' => 'User Management', 'icon' => 'fa fa-users', 'url' => ['profile/index'],],
//                                ['label' => 'Role Management', 'icon' => 'fa fa-dashboard', 'url' => ['role/index'],],
//                                ['label' => 'Module Management', 'icon' => 'fa fa-dashboard', 'url' => ['course-list/index'],],
                            ],
                        ],
                        [
                            'label' => 'System Management',
                            'icon' => 'fa fa-dashboard', 'options' => ['class' => 'delay-child'],
                            'url' => '#',
                            'items' => [
                                ['label' => 'Reset Database', 'icon' => 'fa fa-file-code-o', 'url' => ['migration/reset'],],
                                ['label' => 'Clear Database', 'icon' => 'fa fa-file-code-o', 'url' => ['migration/clear'],],
                                ['label' => 'Demo Database', 'icon' => 'fa fa-file-code-o', 'url' => ['migration/demo'],],
                            ],
                        ],
//                        if user is developer
                        [
                            'label' => 'Dev Tool',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'visible' => Yii::$app->user->isDeveloper, 'options' => ['class' => 'delay-child'],
                            'items' => [
                                ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
//                                [
//                                    'label' => 'Level One',
//                                    'icon' => 'fa fa-circle-o',
//                                    'url' => '#',
//                                    'items' => [
//                                        ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                        [
//                                            'label' => 'Level Two',
//                                            'icon' => 'fa fa-circle-o',
//                                            'url' => '#',
//                                            'items' => [
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                            ],
//                                        ],
//                                    ],
//                                ],
                            ],
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
