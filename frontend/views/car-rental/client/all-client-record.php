<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a(Yii::t('app', 'Register Client'), ['client-registration'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
//            'name',
            'alias',
//            'ic_no',
//            'gender',
//            'race',
//            'address',
            'state',
//            'poscode',
//            'country',
            'phone_no',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'status',
            // 'status_stamp',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view-client-record} {update-client-record} {delete-client-record} {delete} {view}',
                'viewOptions' => ['label'=>'view','url'=>'{view-client-record}'],
                'buttons' => [
                    'delete-client-record' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip','data-method' => 'post','data-pjax' => '0',
                        ]);
                    },
                            'view-client-record' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('yii', 'View'), 'data-toggle' => 'tooltip'
                        ]);
                    },
                            'update-client-record' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip'
                        ]);
                    },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?></div>
