<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RentalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rentals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rental-index">

    <h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
    <?= Html::a(Yii::t('app', 'New Rental'), ['new-rental'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'client.alias',
            'asset.reg_no',
            'start_date',
            'end_date',
             'payment',
             'deposit',
             'penalty',
             'penalty_type',
//             'penalty_description',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'status',
            // 'status_stamp',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view-rental-record} {update-rental-record} {delete-rental-record}',
                'buttons' => [
                    'delete-rental-record' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip', 'data-method' => 'post', 'data-pjax' => '0', //'data-confirm' => Yii::t('kvgrid', 'Are you sure to delete this item?'),
                        ]);
                    },
                            'view-rental-record' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('yii', 'View'), 'data-toggle' => 'tooltip'
                        ]);
                    },
                            'update-rental-record' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip'
                        ]);
                    },
                        ],
                    ],
                ],
            ]);
            ?>
        <?php Pjax::end(); ?></div>
