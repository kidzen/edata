<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rental */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Rental',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rentals'), 'url' => ['all-rental-record']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view-rental-record', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rental-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-rental', [
        'model' => $model,
        'clientArray' => $clientArray,
        'assetArray' => $assetArray,
    ]) ?>

</div>
