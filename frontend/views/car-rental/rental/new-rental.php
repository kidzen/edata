<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Rental */

$this->title = Yii::t('app', 'New Rental');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rentals'), 'url' => ['all-rental-record']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rental-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-rental', [
        'model' => $model,
        'clientArray' => $clientArray,
        'assetArray' => $assetArray,
    ]) ?>

</div>
