<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Rental */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rental-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3">
            <?=
            $form->field($model, 'client_id')->widget(kartik\select2\Select2::className(), [
                'data' => $clientArray,
                'options' => ['placeholder' => 'Select a client ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'asset_id')->widget(kartik\select2\Select2::className(), [
                'data' => $assetArray,
                'options' => ['placeholder' => 'Select an asset ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?=
            $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Start rentdal date & time ...', 'value' => date('Y/m/d H:i:s', time())],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii:ss',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'End rentdal date & time ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii:ss',
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'payment')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'deposit')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'penalty')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'penalty_type')->dropDownList(['', 'summons' => 'summons', 'damage' => 'damage']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'penalty_description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
