<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Asset */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asset',
]) . $model->reg_no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assets'), 'url' => ['all-asset-record']];
$this->params['breadcrumbs'][] = ['label' => $model->reg_no, 'url' => ['view-asset-record', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asset-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-asset', [
        'model' => $model,
    ]) ?>

</div>
