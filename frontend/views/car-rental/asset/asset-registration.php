<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Asset */

$this->title = Yii::t('app', 'Register Asset');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assets'), 'url' => ['all-asset-record']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-asset', [
        'model' => $model,
    ]) ?>

</div>
