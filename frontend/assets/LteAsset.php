<?php

namespace frontend\assets;

use yii\base\Exception;
use yii\web\AssetBundle;

/**
 * AdminLte AssetBundle
 * @since 0.1
 */
class LteAsset extends AssetBundle {

    public $sourcePath = '@frontend/assets/lte-dist';
    public $css = [
//        'css/AdminLTE.min.css',
        'css/AdminLTE.css',
        'css/animate.min.css',
        'css/animate.delay.css',
    ];
    public $js = [
//        'plugins/jQuery/jQuery-2.2.0.min.js',
//        'plugins/jQueryUI/jquery-ui.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
        'js/custom.js',
        'js/app.min.js',
        /////
//        'bootstrap/js/bootstrap.min.js',
//        'plugins/fastclick/fastclick.js',
//        'plugins/sparkline/jquery.sparkline.min.js',
//        'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
//        'plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
//        'plugins/chartjs/Chart.min.js',
//        'js/pages/dashboard2.js',
//        'js/demo.js',
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\JqueryAsset',
//        'yii\jui\JuiAsset',
    ];

    /**
     * @var string|bool Choose skin color, eg. `'skin-blue'` or set `false` to disable skin loading
     * @see https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html#layout
     */
    public $skin = '_all-skins';

    /**
     * @inheritdoc
     */
    public function init() {
        // Append skin color file if specified
        if ($this->skin) {
            if (('_all-skins' !== $this->skin) && (strpos($this->skin, 'skin-') !== 0)) {
                throw new Exception('Invalid skin specified');
            }

            $this->css[] = sprintf('css/skins/%s.min.css', $this->skin);
        }

        parent::init();
    }

}
