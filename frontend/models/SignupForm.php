<?php

namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\Profile;
use yii\db\Expression;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $username;
    public $email;
    public $password;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if (!$this->validate()) {
            return null;
        }

        $user = new User(['scenario'=>'signup']);
        $profile = new Profile();
        $profile->status = Profile::STATUS_ACTIVE;
        $profile->status_stamp = time();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->role = \common\components\User::USER;
        $user->status = User::STATUS_INACTIVE;
        $user->status_stamp = time();
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->validate()) {

            $profile->save(false);
            $user->link('profile', $profile);
//            $user->profile_id = $profile->id;
            $user->save(false);

            return $user;
        } else {
            return null;
        }

        // the following three lines were added:
//        $auth = Yii::$app->authManager;
//        $authorRole = $auth->getRole('author');
//        $auth->assign($authorRole, $user->getId());
//        return $user->save() ? $user : null;
//        return $user;
    }

}
