<?php

namespace frontend\controllers;

use Yii;
use common\models\Client;
use common\models\ClientSearch;
use common\models\Asset;
use common\models\AssetSearch;
use common\models\Rental;
use common\models\RentalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CarRentalController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-client-record' => ['POST'],
                    'delete-asset-record' => ['POST'],
                    'delete-rental-record' => ['POST'],
                ],
            ],
        ];
    }

//    public function actionIndex() {
//        return $this->render('index');
//    }
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//                                  RENTAL ORDER
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//    ----------------------------------------------------------------------------------------------------------------------------------------------
    public function actionAllRentalRecord() {
        $searchModel = new RentalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('rental\all-rental-record', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rental model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewRentalRecord($id) {
        return $this->render('rental\view-rental-record', [
                    'model' => $this->findRentalRecord($id),
        ]);
    }

    /**
     * Creates a new Rental model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNewRental() {
        $model = new Rental();
        $client = Client::find()->asArray()->all();
        $asset = Asset::find()->asArray()->all();
        $clientArray = \yii\helpers\ArrayHelper::map($client, 'id', 'alias');
        $assetArray = \yii\helpers\ArrayHelper::map($asset, 'id', 'reg_no');
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view-rental-record', 'id' => $model->id]);
            }
        }
        return $this->render('rental\new-rental', [
                    'model' => $model,
                    'clientArray' => $clientArray,
                    'assetArray' => $assetArray,
        ]);
    }

    /**
     * Updates an existing Rental model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateRentalRecord($id) {
        $model = $this->findRentalRecord($id);
        $client = Client::find()->asArray()->all();
        $asset = Asset::find()->asArray()->all();
        $clientArray = \yii\helpers\ArrayHelper::map($client, 'id', 'alias');
        $assetArray = \yii\helpers\ArrayHelper::map($asset, 'id', 'reg_no');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view-rental-record', 'id' => $model->id]);
            }
        }
        return $this->render('rental\update-rental-record', [
                    'model' => $model,
                    'clientArray' => $clientArray,
                    'assetArray' => $assetArray,
        ]);
    }

    /**
     * Deletes an existing Rental model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteRentalRecord($id) {
        $this->findRentalRecord($id)->delete();

        return $this->redirect(['all-rental-record']);
    }

    /**
     * Finds the Rental model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rental the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRentalRecord($id) {
        if (($model = Rental::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    ----------------------------------------------------------------------------------------------------------------------------------------------
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//                                  CLIENT
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//    ----------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionAllClientRecord() {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('client\all-client-record', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewClientRecord($id) {
        return $this->render('client\view-client-record', [
                    'model' => $this->findClientRecord($id),
        ]);
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionClientRegistration() {
        $model = new Client();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-client-record', 'id' => $model->id]);
        } else {
            return $this->render('client\client-registration', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateClientRecord($id) {
        $model = $this->findClientRecord($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-client-record', 'id' => $model->id]);
        } else {
            return $this->render('client\update-client-record', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Client model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteClientRecord($id) {
        $this->findClientRecord($id)->delete();

        return $this->redirect(['all-client-record']);
    }

    public function actionDelete($id) {
        $this->findClientRecord($id)->delete();

        return $this->redirect(['all-client-record']);
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findClientRecord($id) {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    ----------------------------------------------------------------------------------------------------------------------------------------------
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//                                  ASSET
//    ----------------------------------------------------------------------------------------------------------------------------------------------
//    ----------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Lists all Asset models.
     * @return mixed
     */
    public function actionAllAssetRecord() {
        $searchModel = new AssetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('asset\all-asset-record', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Asset model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewAssetRecord($id) {
        return $this->render('asset\view-asset-record', [
                    'model' => $this->findAssetRecord($id),
        ]);
    }

    /**
     * Creates a new Asset model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAssetRegistration() {
        $model = new Asset();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-asset-record', 'id' => $model->id]);
        } else {
            return $this->render('asset\asset-registration', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Asset model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateAssetRecord($id) {
        $model = $this->findAssetRecord($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-asset-record', 'id' => $model->id]);
        } else {
            return $this->render('asset\update-asset-record', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Asset model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteAssetRecord($id) {
        $this->findAssetRecord($id)->delete();

        return $this->redirect(['all-asset-record']);
    }

    /**
     * Finds the Asset model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Asset the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAssetRecord($id) {
        if (($model = Asset::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
