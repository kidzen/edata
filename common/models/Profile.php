<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $gender
 * @property string $race
 * @property string $address
 * @property string $state
 * @property integer $poscode
 * @property string $country
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $status
 * @property integer $status_stamp
 *
 * @property User[] $users
 */
class Profile extends \yii\db\ActiveRecord {

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['poscode', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status', 'status_stamp'], 'integer'],
            [['name', 'alias', 'gender', 'race', 'address', 'state', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'gender' => Yii::t('app', 'Gender'),
            'race' => Yii::t('app', 'Race'),
            'address' => Yii::t('app', 'Address'),
            'state' => Yii::t('app', 'State'),
            'poscode' => Yii::t('app', 'Poscode'),
            'country' => Yii::t('app', 'Country'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'status_stamp' => Yii::t('app', 'Status Stamp'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasOne(User::className(), ['profile_id' => 'id']);
    }

}
