<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "asset".
 *
 * @property integer $id
 * @property integer $type
 * @property string $model
 * @property string $reg_no
 * @property string $description
 * @property string $notes
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $status
 * @property integer $status_stamp
 *
 * @property Rental[] $rentals
 */
class Asset extends \yii\db\ActiveRecord {

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'asset';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status', 'status_stamp'], 'integer'],
            [['model', 'reg_no', 'description', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'model' => Yii::t('app', 'Model'),
            'reg_no' => Yii::t('app', 'Reg No'),
            'description' => Yii::t('app', 'Description'),
            'notes' => Yii::t('app', 'Notes'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'status_stamp' => Yii::t('app', 'Status Stamp'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentals() {
        return $this->hasMany(Rental::className(), ['asset_id' => 'id']);
    }

}
