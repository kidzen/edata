<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rental;

/**
 * RentalSearch represents the model behind the search form about `common\models\Rental`.
 */
class RentalSearch extends Rental
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'asset_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status', 'status_stamp'], 'integer'],
            [['payment', 'deposit', 'penalty'], 'number'],
            [['penalty_type', 'penalty_description','start_date', 'end_date', ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rental::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['id'=>SORT_DESC];
//        var_dump($dataProvider->params);die();
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'asset_id' => $this->asset_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'payment' => $this->payment,
            'deposit' => $this->deposit,
            'penalty' => $this->penalty,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
            'status_stamp' => $this->status_stamp,
        ]);

        $query->andFilterWhere(['like', 'penalty_type', $this->penalty_type])
            ->andFilterWhere(['like', 'penalty_description', $this->penalty_description]);

        return $dataProvider;
    }
}
