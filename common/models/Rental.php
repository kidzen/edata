<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rental".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $asset_id
 * @property integer $start_date
 * @property integer $end_date
 * @property double $payment
 * @property double $deposit
 * @property double $penalty
 * @property string $penalty_type
 * @property string $penalty_description
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $status
 * @property integer $status_stamp
 *
 * @property Asset $asset
 * @property Client $client
 */
class Rental extends \yii\db\ActiveRecord {

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'rental';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['client_id', 'asset_id', 'start_date'], 'required'],
            [['client_id', 'asset_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status', 'status_stamp'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['payment', 'deposit', 'penalty'], 'number'],
            [['penalty_type', 'penalty_description'], 'string', 'max' => 255],
            [['asset_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asset::className(), 'targetAttribute' => ['asset_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            ['end_date', 'compare', 'compareAttribute' => 'start_date', 'operator' => '>=',
                'enableClientValidation' => true, 'message' => '"End Date" must not before than "Start Date"'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'client_id' => Yii::t('app', 'Client ID'),
            'asset_id' => Yii::t('app', 'Asset ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'payment' => Yii::t('app', 'Payment'),
            'deposit' => Yii::t('app', 'Deposit'),
            'penalty' => Yii::t('app', 'Penalty'),
            'penalty_type' => Yii::t('app', 'Penalty Type'),
            'penalty_description' => Yii::t('app', 'Penalty Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'status_stamp' => Yii::t('app', 'Status Stamp'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset() {
        return $this->hasOne(Asset::className(), ['id' => 'asset_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient() {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @inheritdoc
     */
//    public function beforeSave($insert) {
//        if (parent::beforeSave($insert)) {
////            if ($this->isNewRecord || (!$this->isNewRecord && ($this->start_date || $this->end_date))) {
//            if ($this->start_date)
//                $this->start_date = date('d-M-y H:i:s', $this->start_date);
//            if ($this->end_date)
//                $this->end_date = date('d-M-y H:i:s', $this->end_date);
////            }
//            return true;
//        }
//        return false;
//    }
}
