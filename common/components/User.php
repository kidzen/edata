<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

class User extends \yii\web\User {

    const ADMINSTRATOR = 1;
    const ADMIN_SYSTEM = 2;
    const USER = 3;
    const CLIENT = 4;
    const DEVELOPER = 5;

    public function getName() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getName() : 'Guest';
    }

    public function getJoinDate() {
        $identity = $this->getIdentity();

//        return gmdate("Y-m-d\TH:i:s\Z", $timestamp);
//        return date("d-m-Y\TH:i:s\Z", $timestamp);

        return $identity !== null ? date("M. Y", $identity->getJoinDate()) : null;
    }

    public function getProfileId() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getProfileId() : null;
    }

    public function getRole() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getRole() : null;
    }

    public function getRoleName() {
        $identity = $this->getRole();
        if ($identity !== null) {
            if ($identity == self::ADMINSTRATOR) {
                return 'Administrator';
            } else if ($identity == self::ADMIN_SYSTEM) {
                return 'Admin System';
            } else if ($identity == self::USER) {
                return 'User';
            } else if ($identity == self::CLIENT) {
                return 'Client';
            } else if ($identity == self::DEVELOPER) {
                return 'Developer';
            } else {
                return 'Unregistered';
            }
        } else {
            return 'Guest';
        }
    }

    public function getIsAdmin() {
        $identity = $this->getRole();

        return $identity == self::ADMINSTRATOR ? true : false;
    }

    public function getIsAdminSys() {
        $identity = $this->getRole();

        return $identity == self::ADMIN_SYSTEM ? true : false;
    }

    public function getIsUser() {
        $identity = $this->getRole();

        return $identity == self::USER ? true : false;
    }

    public function getIsClient() {
        $identity = $this->getRole();

        return $identity == self::CLIENT ? true : false;
    }

    public function getIsDeveloper() {
        $identity = $this->getRole();

        return $identity == self::DEVELOPER ? true : false;
    }

}
