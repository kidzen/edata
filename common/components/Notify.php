<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;

class Notify {

    public $message;
    public $duration;
    
    public function fail($message,$duration = 3000) {
        return Yii::$app->session->setFlash('error', [
                    'type' => 'danger',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' GAGAL.',
                    'message' => $message
        ]);
    }

    public function success($message,$duration = 3000) {
        return Yii::$app->session->setFlash('success', [
                    'type' => 'success',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-check-sign',
                    'title' => ' BERJAYA.',
                    'message' => $message
        ]);
    }

}
