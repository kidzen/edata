<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'eData',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
//        'authManager' => [
//            'class' => 'yii\rbac\DbManager',
//        ],
//        'aliases' => [
//            '@mdm/admin' => '@frontend/extensions/mdm/yii2-admin-2.0.0',
//        // for example: '@mdm/admin' => '@frontend/extensions/mdm/yii2-admin-2.0.0',
//        ],
    ],
];
