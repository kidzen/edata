<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
//    public function actionInit()
//    {
//        $auth->removeAll(); //remove previous rbac.php files under console/data
// 
//        //CREATE PERMISSIONS		
//	//Permission to create users
//	$createUsers = $auth->createPermission('createUsers');
//        $createUsers->description = 'Create Users';
//        $auth->add($createUsers);
// 
//	//Permission to edit user profile
//	$editUserProfile = $auth->createPermission('editUserProfile');
//        $editUserProfile->description = 'Edit User Profile';
//        $auth->add($editUserProfile);
// 
//	//APPLY THE RULE
//	$rule = new UserRoleRule(); //Apply our Rule that use the user roles from user table
//	$auth->add($rule);
// 
//	//ROLES AND PERMISSIONS
//        //user role
//	$user = $auth->createRole('user');  //user role
//	$user->ruleName = $rule->name;
//	$auth->add($user); 
//	// ... add permissions as children of $user ...
//        //none in this example
// 
//	//editor role
//	$editor = $auth->createRole('editor');
//	$editor->ruleName = $rule->name;
//	$auth->add($editor);
//	// ... add permissions as children of $editor ..
//        $auth->addChild($editor, $user); //user is a child of editor
//	$auth->addChild($editor, $editUserProfile); //editor can edit profiles
// 
//	//Admin role
//	$admin = $auth->createRole('admin');
//	$admin->ruleName = $rule->name;
//	$auth->add($admin);
//	$auth->addChild($admin, $editor); //editor is child of admin, for consequence user is also child of admin
//	// ... add permissions as children of $admin ..
//	$auth->addChild($admin, $createUsers); //admin role can create users and also edit users because is parent of editor
//    
//    }
    
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // add "createPost" permission
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        // add "author" role and give this role the "createPost" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($admin, 1);
        $auth->assign($author, 2);
        
    }
}