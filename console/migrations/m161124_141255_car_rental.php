<?php

use yii\db\Migration;

class m161124_141255_car_rental extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        //asset table
        $this->createTable('{{%asset}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'model' => $this->string(),
            'reg_no' => $this->string(),
            'description' => $this->string(),
            'notes' => $this->string(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'status_stamp' => $this->integer(),
                ], $tableOptions);
        //client table
        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'ic_no' => $this->string()->notNull(),
            'gender' => $this->string(),
            'race' => $this->string(),
            'address' => $this->string(),
            'state' => $this->string(),
            'poscode' => $this->integer(),
            'country' => $this->string(),
            'phone_no' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'status_stamp' => $this->integer(),
                ], $tableOptions);
        //rental table
        $this->createTable('{{%rental}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'asset_id' => $this->integer()->notNull(),
            'start_date' => $this->dateTime()->notNull(),
            'end_date' => $this->dateTime(),
            'payment' => $this->decimal(10,2),
            'deposit' => $this->decimal(10,2),
            'penalty' => $this->decimal(10,2),
            'penalty_type' => $this->string(),
            'penalty_description' => $this->string(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'status_stamp' => $this->integer(),
                ], $tableOptions);
        $this->addForeignKey('fk_rental_client_id', '{{%rental}}', 'client_id', '{{%client}}', 'id');
        $this->addForeignKey('fk_rental_asset_id', '{{%rental}}', 'asset_id', '{{%asset}}', 'id');
        $this->insertDemoData($tableOptions);
    }

    public function insertDemoData($tableOptions) {
        $this->insert('{{%asset}}', [
            'id' => 1,
            'type' => 1,
            'model' => 'Proton',
            'reg_no' => 'TBA 1234',
            'description' => 'Merah Tahun 2014',
            'notes' => 'Rodtax bulan 3',
            'created_at' => time(),
            'created_by' => 1,
            'updated_at' => time(),
            'updated_by' => 1,
                ], $tableOptions);
        //client table
        $this->insert('{{%client}}', [
            'id' => 1,
            'name' => 'Aziz bin Abdullah',
            'alias' => 'Aziz',
            'ic_no' => '910101-01-5233',
            'gender' => 'Male',
            'race' => 'Malay',
            'address' => 'Lorong 1',
            'state' => 'Kuala Terengganu',
            'poscode' => '50020',
            'country' => 'Malaysia',
            'phone_no' => '012-2656765',
            'created_at' => time(),
            'created_by' => 1,
            'updated_at' => time(),
            'updated_by' => 1,
                ], $tableOptions);
    }

    public function down() {
        $this->dropForeignKey('fk_rental_client_id', '{{%rental}}');
        $this->dropForeignKey('fk_rental_asset_id', '{{%rental}}');
        $this->dropTable('{{%asset}}');
        $this->dropTable('{{%client}}');
        $this->dropTable('{{%rental}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
