<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'role' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'status_stamp' => $this->integer()->notNull(),
            ], $tableOptions);

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'alias' => $this->string(),
            'gender' => $this->string(),
            'race' => $this->string(),
            'address' => $this->string(),
            'state' => $this->string(),
            'poscode' => $this->integer(),
            'country' => $this->string(),

            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(0),
            'status_stamp' => $this->integer(),
            ], $tableOptions);
        $this->addForeignKey('fk_user_profile_id', '{{%user}}', 'profile_id', '{{%profile}}', 'id');
        $this->userdata($tableOptions);

    }

    public function userdata($tableOptions)
    {
        $this->insert('{{%profile}}', [
            'id'=>1,
            'created_at' => time(),
            'created_by' => 1,
            'updated_at' => time(),
            'updated_by' => 1,
            'status' => 1,
            'status_stamp' => time(),
            ], $tableOptions);
        $this->insert('{{%user}}', [
            'id'=>1,
            'profile_id'=>1,
            'username' => 'admin1',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
            'email' => 'admin1@mail.com',
            
            'role' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'status' => 1,
            'status_stamp' => time(),
            ], $tableOptions);

        $this->insert('{{%profile}}', [
            'id'=>2,
            'created_at' => time(),
            'created_by' => 2,
            'updated_at' => time(),
            'updated_by' => 2,
            'status' => 2,
            'status_stamp' => time(),
            ], $tableOptions);
        $this->insert('{{%user}}', [
            'id'=>2,
            'profile_id'=>2,
            'username' => 'admin2',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin2'),
            'email' => 'admin2@mail.com',
            
            'role' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'status' => 1,
            'status_stamp' => time(),
            ], $tableOptions);
        
    }
    public function down()
    {
        $this->dropForeignKey('fk_user_profile_id', '{{%user}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%profile}}');
    }
}


